import React from 'react';

import View from '../../containers/View';

const CollectionIndex = props => (
  <View sidebarTitle="Conversations" title="Message analyser">
    {props.match.params.id}
  </View>
);

export default CollectionIndex;
