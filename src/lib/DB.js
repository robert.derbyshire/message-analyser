import Dexie from 'dexie';

class DB {
  constructor() {
    this.collectionsDB = new Dexie('collectionsDB');

    this.collectionsDB.version(1).stores({
      list: '++collection_id',
      raw: 'collection_id',
    });
  }

  getCollections() {
    return this.collectionsDB.list.toCollection().toArray().then(
      collections => Promise.all(collections.map(async collection => ({
        exists: await Dexie.exists(DB.getDBName(collection.collection_id)),
        ...collection,
      }))),
    );
  }

  newCollection(owner, fromDate, untilDate, raw) {
    return this.collectionsDB.list.add({
      owner,
      fromDate,
      untilDate,
    }).then(
      id => this.collectionsDB.raw.add({ raw, collection_id: id }),
    );
  }

  static getDBName(id) {
    return `analyserDB${id}`;
  }

  async getDB(id) {
    const dbName = DB.getDBName(id);
    const db = new Dexie(dbName);

    db.version(1).stores({
      messages: '++,thread_id,person_id,timestamp,type',
      threads: '++thread_id,type,isArchived,isActiveparticipants', // contains participants and other data
      participants: '++person_id,isOwner,isPlaceholder', // contains name, special flags and a list of threads
      people: '++id',
    });

    this.currentDB = db;

    return db;
  }

  async deleteDB(id) {
    const dbName = DB.getDBName(id);

    this.currentDB = null;

    if (await Dexie.exists(dbName)) await Dexie.delete(dbName);

    return Promise.all([
      this.collectionsDB.list.delete(id),
      this.collectionsDB.raw.delete(id),
    ]);
  }
}

export default new DB();
