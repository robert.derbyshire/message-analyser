import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import red from '@material-ui/core/colors/red';
import grey from '@material-ui/core/colors/grey';

const style = {
  container: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    left: 0,
    right: 0,
    textAlign: 'center',
  },
};

const CenterMessage = (props) => {
  const { classes } = props;
  let { color } = props;
  let styleColor = 'inherit';

  if (color === 'error') {
    styleColor = red[500];
  } else if (color === 'empty') {
    styleColor = grey[300];
  } else if (color === 'default') {
    styleColor = grey[900];
  }

  if (!['primary', 'secondary'].includes(props.color)) {
    color = 'inherit';
  }

  const icon = props.icon ? React.cloneElement(props.icon, { color, style: { color: styleColor, fontSize: 48 } }) : null;
  const textStyle = color === 'disabled' ? { color: 'rgba(0, 0, 0, 0.26)' } : {};

  return (
    <div className={classes.container}>
      {icon}
      <Typography style={{ color: styleColor }} variant="h5" color={color}>{props.message}</Typography>
    </div>
  );
};

CenterMessage.propTypes = {
  message: PropTypes.string.isRequired,
  icon: PropTypes.node,
  color: PropTypes.oneOf(['primary', 'secondary', 'error', 'empty', 'default']),
};

CenterMessage.defaultMessage = {
  color: 'disabled',
};

export default withStyles(style)(CenterMessage);
