import React from 'react';

import View from '../../containers/View';
import MessageETL from '../../lib/MessageETL';
import DB from '../../lib/DB';

import OverviewSidebar from './OverviewSidebar';

import CenterMessage from '../../components/CenterMessage';

export default class OverviewIndex extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      collections: [],
      refreshingCollections: true,
    };

    this.refreshCollections = this.refreshCollections.bind(this);
    this.deleteCollection = this.deleteCollection.bind(this);
    this.newCollection = this.newCollection.bind(this);
  }

  componentDidMount() {
    this.refreshCollections();
  }

  refreshCollections() {
    this.setState({ refreshingCollections: true });
    DB.getCollections().then((collections) => {
      this.setState({
        collections,
        refreshingCollections: false,
      });
    });
  }

  deleteCollection(id) {
    DB.deleteDB(id).then(this.refreshCollections);
  }

  newCollection(file) {
    MessageETL.doLoad(file).then(this.refreshCollections);
  }

  render() {
    const { collections, refreshingCollections } = this.state;
    return (
      <View
        title="Message analyser"
        sidebarTitle="Message collections"
        sidebar={(
          <OverviewSidebar
            onChange={this.newCollection}
            onDeleteCollection={this.deleteCollection}
            collections={collections}
            refreshing={refreshingCollections}
          />
        )}
      >
        <CenterMessage message="No collection selected" color="empty" />
      </View>
    );
  }
}
