import React from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';

import InvalidRoute from './views/InvalidRoute';
import OverviewIndex from './views/overview/OverviewIndex';
import CollectionIndex from './views/collections/CollectionIndex';

import './App.css';

const App = () => (
  <div className="App">
    <Switch>
      <Redirect exact from="/" to="/collections" />
      <Route exact path="/collections" component={OverviewIndex} />
      <Route exact path="/collections/:id" component={CollectionIndex} />
      <Route path="/" component={InvalidRoute} />
    </Switch>
  </div>
);

export default App;
