import Dexie from 'dexie';

class AnalyserDB {
  constructor(id) {
    const db = new Dexie(`analysedrDB${id}`);

    db.version(1).stores({
      messages: '++message_id,thread_id,sender_id,timestamp,type',
      threads: 'thread_id,participants',
      participants: 'person_id,thread_id',
      people: '++id',
    });

    this.db = db;
  }
}

export default function loadDB(id) {
  return new AnalyserDB(id).db;
}
