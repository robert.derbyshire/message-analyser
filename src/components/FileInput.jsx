import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AttachFileIcon from '@material-ui/icons/AttachFile';

const style = {
  input: {
    display: 'none',
  },
  fab: {
    position: 'absolute',
    bottom: '16px',
    right: '16px',
  },
};

class FileInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filePath: '',
      fileName: '',
    };
  }

  onChange = (e) => {
    const filePath = e.target.value;
    let fileName = '';

    if (filePath) {
      const startIndex = (filePath.indexOf('\\') >= 0 ? filePath.lastIndexOf('\\') : filePath.lastIndexOf('/'));
      fileName = filePath.substring(startIndex);

      if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0) {
        fileName = fileName.substring(1);
      }
    }

    this.setState({
      filePath,
      fileName,
    });

    this.props.onChange(e.target.files[0]);
  }

  setHover = isHovering => this.setState({ hover: isHovering })

  render() {
    const { classes } = this.props;

    return (
      <div>
        <input
          accept=".zip"
          className={classes.input}
          id="raised-button-file"
          type="file"
          onChange={this.onChange}
        />
        <label htmlFor="raised-button-file">
          <Button className={classes.fab} component="span" variant="contained" color="secondary" aria-label="attach_file">
            {this.props.text}
            <AttachFileIcon />
          </Button>
        </label>
      </div>
    );
  }
}

export default withStyles(style)(FileInput);

FileInput.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
