import DB from './DB';

const load = async (collection) => {
  DB.newCollection(
    collection.owner,
    collection.oldestMessageTimestamp,
    collection.latestMessageTimestamp,
    collection,
  ).then(
    async (collectionId) => {
      const analyserDB = await DB.getDB(collectionId);

      const people = new Map(Array.from(new Set(collection.threads.map(
        thread => thread.participants,
      ).flat())).map(person => ([
        person, {
          threads: [],
          isOwner: person === collection.owner,
          isPlaceholder: person === 'Facebook User',
        },
      ])));

      await Promise.all(collection.threads.map(({
        title,
        participants,
        removedParticipants,
        isArchived,
        type,
        isActive,
        messages,
      }) => analyserDB.threads.add({
        title, participants, removedParticipants, isArchived, type, isActive,
      }).then((threadId) => {
        participants.forEach(person => people.get(person).threads.push(threadId));

        return messages.map(message => (
          Object.assign({}, message, {
            threadId,
          })
        ));
      }))).then(messages => analyserDB.messages.bulkAdd(messages.flat()));
      return analyserDB;
    },
  );
};

export default load;
