import React from 'react';
import PropTypes from 'prop-types';


import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from '../components/Sidebar';

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  inner: {
    position: 'relative',
    width: '100%',
    padding: 8,
  },
  toolbar: theme.mixins.toolbar,
});

const View = (props) => {
  const {
    classes, title, sidebarTitle, sidebar, children,
  } = props;

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="absolute">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <Sidebar className={classes.sidebar} title={sidebarTitle}>
        {sidebar}
      </Sidebar>
      <div className={classes.inner}>
        <div className={classes.toolbar} />
        {children}
      </div>
    </div>
  );
};

View.propTypes = {
  title: PropTypes.string.isRequired,
  sidebarTitle: PropTypes.string.isRequired,
  sidebar: PropTypes.node.isRequired,
};

export default withStyles(styles)(View);
