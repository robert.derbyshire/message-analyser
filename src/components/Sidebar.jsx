import React from 'react';

import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  sidebar: {
    position: 'relative',
    padding: '16px',
    width: 300,
    height: '100vh',
    boxSizing: 'border-box',
  },
  toolbar: theme.mixins.toolbar,
});

const Sidebar = (props) => {
  const { classes, children, title } = props;

  return (
    <Drawer
      classes={{
        paper: classes.sidebar,
      }}
      variant="permanent"
    >
      <div className={classes.toolbar} />
      <Typography variant="h5" gutterBottom>{title}</Typography>
      {children}
    </Drawer>
  );
};

export default withStyles(styles)(Sidebar);
