import React from 'react';
import SentimentDissatisfied from '@material-ui/icons/SentimentDissatisfied';
import View from '../containers/View';


import CenterMessage from '../components/CenterMessage';

const InvalidRoute = () => (
  <CenterMessage message="404 Not found" color="disabled" icon={<SentimentDissatisfied />} />
);

export default InvalidRoute;
