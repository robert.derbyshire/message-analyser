import React from 'react';

import FileInput from '../../components/FileInput';
import CollectionsList from '../../components/CollectionsList';

export default props => (
  <div>
    <CollectionsList collections={props.collections} onDeleteCollection={props.onDeleteCollection} />
    <FileInput id="import_collection" onChange={props.onChange} />
  </div>
);
