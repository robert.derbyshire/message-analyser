import React from 'react';

import Paper from '@material-ui/core/Paper';
import View from '../../containers/View';
import MessageETL from '../../lib/MessageETL';
import DB from '../../lib/DB';

import CollectionsSidebar from './CollectionsSidebar';


export default class Overview extends React.Component {
  constructor(props) {
    super(props);

    this.state = { collections: [] };
  }

  componentDidMount() {
    this.refreshCollections();
  }

  refreshCollections = () => {
    DB.getCollections().then((collections) => {
      this.setState({
        collections,
      });
    });
  }

  deleteCollection = (id) => {
    console.log('Deleting', id);
    DB.deleteDB(id).then(this.refreshCollections);
  }

  newCollection = (file) => {
    MessageETL.doLoad(file).then(this.refreshCollections);
  }

  render() {
    return (
      <View
        title="Message analyser"
        sidebarTitle="Message collections"
        sidebar={(
          <CollectionsSidebar
            onChange={this.newCollection}
            onDeleteCollection={this.deleteCollection}
            collections={this.state.collections}
          />
)}
      >
        <Paper />
      </View>
    );
  }
}
