import { zip } from './zip';
import zWorker from './z-worker';
import pako from './pako_inflate.min.worker';
import codecs from './codecs.worker';

zip.workerScripts = {
  inflater: [zWorker, pako, codecs],
  deflater: [zWorker, pako, codecs],
};

export default zip;
