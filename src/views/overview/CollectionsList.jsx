import React from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import CircularProgress from '@material-ui/core/CircularProgress';

import CenterMessage from '../../components/CenterMessage';
import CollectionListItem from './CollectionListItem';

const CollectionsList = ({ refreshing, collections, onDeleteCollection }) => {
  if (refreshing) {
    return <CenterMessage message="Refreshing collections" icon={<CircularProgress color="secondary" />} color="primary" />;
  } if (collections.length) {
    return (
      <List>
        {collections.map(collection => (
          <CollectionListItem
            key={collection.collection_id}
            exists={collection.exists}
            id={collection.collection_id}
            name={collection.owner}
            importDate={collection.untilDate}
            onDeleteCollection={onDeleteCollection}
          />
        ))}
      </List>
    );
  }
  return <CenterMessage message="No collections" color="empty" />;
};

CollectionsList.propType = {
  collections: PropTypes.array,
};

CollectionsList.defaultProps = {
  collections: [],
};

export default CollectionsList;
