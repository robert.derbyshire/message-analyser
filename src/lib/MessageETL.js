import Extract from './MessageExtractor';
import Transform from './MessageTransformer';
import Load from './MessageLoader';

function isZip(fileType) {
  return ([
    'application/zip',
    'application/x-zip',
    'application/x-zip-compressed',
    'application/octet-stream', 'application/x-compress', 'application/x-compressed', 'multipart/x-zip']).includes(fileType);
}

async function doLoad(file) {
  if (isZip(file.type)) {
    return Extract(file).then(Transform).then(Load);
  }
  throw TypeError(`Invalid file type, ${file.type}`);
}

export default { doLoad };
