import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListAltIcon from '@material-ui/icons/List';
import DeleteIcon from '@material-ui/icons/Delete';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';

import moment from 'moment';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const CollectionListItem = ({
  exists, id, onDeleteCollection, importDate, name,
}) => {
  const link = exists ? Link : '';
  console.log(id);

  return (
    <ListItem disabled={!exists} disableGutters to={`/collections/${id}`} component={link} key={id}>
      <ListItemIcon>
        <ListAltIcon />
      </ListItemIcon>
      <ListItemText primary={name} secondary={moment(importDate).format('MMMM Do, YYYY')} />
      <ListItemSecondaryAction onClick={() => onDeleteCollection(id)}>
        <IconButton>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default CollectionListItem;

CollectionListItem.propTypes = {
  name: PropTypes.string.isRequired,
  importDate: PropTypes.instanceOf(Date).isRequired,
  id: PropTypes.number.isRequired,
};
