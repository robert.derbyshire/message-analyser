import zip from '../vendors/zip/index';

const extract = async (zipFile) => {
  const reader = await new Promise((resolve) => {
    zip.createReader(new zip.BlobReader(zipFile), resolve);
  });

  const threadEntries = await new Promise(resolve => reader.getEntries((entries) => {
    resolve(entries.filter(({ filename }) => filename.startsWith('messages') && filename.endsWith('.json')));
  }));

  if (threadEntries.length === 0) {
    reader.close();
    throw TypeError('Invalid file provided');
  } else {
    const threads = await Promise.all(
      threadEntries.map(thread => new Promise((resolve) => {
        thread.getData(new zip.TextWriter(), (text) => {
          resolve(JSON.parse(text));
        });
      })),
    );

    reader.close();

    console.log('Extracted', threads);

    return threads;
  }
};

export default extract;
