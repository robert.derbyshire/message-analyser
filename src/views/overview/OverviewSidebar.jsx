import React from 'react';

import FileInput from '../../components/FileInput';
import CollectionsList from './CollectionsList';

const OverviewSidebar = ({
  collections, onDeleteCollection, refreshing, onChange,
}) => (
  <div>
    <CollectionsList
      collections={collections}
      onDeleteCollection={onDeleteCollection}
      refreshing={refreshing}
    />
    <FileInput id="import_collection" text="New collection" onChange={onChange} />
  </div>
);


export default OverviewSidebar;
