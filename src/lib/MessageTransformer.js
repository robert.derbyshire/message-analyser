const transform = async (threads) => {
  const data = {
    latestMessageTimestamp: 0,
    oldestMessageTimestamp: Infinity,
    threads: [],
  };

  const ownerCandidates = {};

  data.threads = threads.map((thread) => {
    const participants = new Set(thread.participants.map(participant => participant.name));
    const removedParticipants = new Set();

    if (participants.has('')) {
      participants.delete('');
      participants.add('Facebook User');
    }

    const messages = thread.messages.map((message) => {
      const timestamp = new Date(message.timestamp_ms);
      const sender = (message.sender_name === undefined || message.sender_name === '') ? 'Facebook User' : message.sender_name;

      if (!participants.has(sender)) {
        removedParticipants.add(sender);
      }

      return Object.assign({}, message, {
        timestamp,
        sender_name: sender,
      });
    });

    [...participants, ...removedParticipants].forEach((candidate) => {
      ownerCandidates[candidate] = ownerCandidates[candidate] ? ownerCandidates[candidate] + 1 : 1;
    });

    const latestTimestampInThread = messages[0].timestamp;
    const oldestTimestampInThread = messages[thread.messages.length - 1].timestamp;

    if (latestTimestampInThread > data.latestMessageTimestamp) {
      data.latestMessageTimestamp = latestTimestampInThread;
    }

    if (oldestTimestampInThread < data.oldestMessageTimestamp) {
      data.oldestMessageTimestamp = oldestTimestampInThread;
    }

    return Object.assign({}, thread, {
      messages,
      removedParticipants: Array.from(removedParticipants),
      participants: Array.from(participants).concat(Array.from(removedParticipants)),
      archived: thread.thread_path.split('/')[0] === 'archived_threads',
      type: thread.thread_type === 'Regular' ? 'Personal' : 'Group',
      active: thread.is_still_participant,
    });
  });

  data.owner = Object.keys(ownerCandidates).reduce(
    (a, b) => (ownerCandidates[a] > ownerCandidates[b] ? a : b),
  );

  data.latestMessageTimestamp = new Date(data.latestMessageTimestamp);
  data.oldestMessageTimestamp = new Date(data.oldestMessageTimestamp);

  console.log('Transformed', data);
  return data;
};

export default transform;
