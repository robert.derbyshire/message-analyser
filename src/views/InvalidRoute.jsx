import React from 'react';
import SentimentDissatisfied from '@material-ui/icons/SentimentDissatisfied';
import View from '../containers/View';


import CenterMessage from '../components/CenterMessage';

const InvalidRoute = () => (
  <View title="Message analyser">
    <CenterMessage message="404 Not found" color="error" icon={<SentimentDissatisfied />} />
  </View>
);

export default InvalidRoute;
